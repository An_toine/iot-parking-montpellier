# Projet IOT - Parking Montpellier

**Groupe :** Antoine Barbier et Jérémy Simione

**Matériel requis :** ESP8266

---

## Sujet du projet

L'objectif du projet est d'implémenter un programme pour ESP8266/ESP32 permettant à partir de données de géolocalisation, de trouver les parkings de Montpellier 3M les plus proches, ayant des places disponibles.

Pour cela, vous utiliserez les données disponibles sur la plateforme de données ouvertes : <https://data.montpellier3m.fr/>.
Dans une version simplifiée, la notion de proximité pourra se baser sur la distance en ligne droite entre vos données de géolocalisation et les parkings. Dans une version plus élaborée, vous pourrez utiliser une API libre comme par exemple celle du projet Open Source Routing Matching (<http://project-osrm.org/>).

Le projet est à rendre en binôme, sous forme d'une archive contenant vos codes sources, ainsi qu'un rapport, au format PDF (et rédigé en LaTeX) décrivant d'une part l'approche générale de votre calcul, mais également -- et surtout -- les aspects liés à la gestion de votre microcontrôleur (erreurs, autonomie, configuration, sécurité, mise à jour, etc.).
Vous mentionnerez dans un fichier tabulé intitulé AUTHORS la composition de votre binôme (numéro étudiant, prénom, nom ; un étudiant par ligne).

---

## Informations

La localisation de l'utilisateur est écrite en dure dans le programme étant donnée que L'ESP utilisé pour ce projet n'est pas doté de modules GPS.

## Configuration du programme avant de le téléverser

il faut définir le point d'accès wifi de l'ESP.
Pour cela, il faut créer et définir un fichier `config.h` avec le contenu suivant.

```C
#ifndef STASSID
#define STASSID "WIFI_NAME"
#define STAPSK "WIFI_PASSWORD"
#endif
```

Le fichier `config.h` doit être placé à la racine du répertoire `src`.
