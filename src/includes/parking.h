#ifndef PARKING_H
#define PARKING_H

#include <Arduino.h>
#include <string>

// Class Parking
class Parking
{
public:
    String id;
    String name;
    float longitude;
    float lattitude;
    float distance; // Distance entre le parking et l'utilisateur
    int nbPlacesDispo;

    // constructors
    Parking(String id, String name, float longitude, float lattitude);

    //methods
    String getUrlXML();

    // comparator
    bool operator<(Parking const &other);
};

#endif