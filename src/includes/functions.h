#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <Arduino.h>
#include <ArduinoOTA.h>
#include <string.h>
#include <math.h>

// Conversion des degrés en radian
double convertRad(double input);

// Fonction qui calcule la distance à vol d'oiseau entre deux coordonnées GPS (longitude et lattitude)
double Distance(float lat_a_degre, float lon_a_degre, float lat_b_degre, float lon_b_degre);

// Fonction qui récupère le nombre de place sur un parking
int getRemainingPlaces(String payload);

// Fonction qui parse le paramètre payload pour récupérer la distance entre l'utilisateur et le parking
double getDistanceBetweenPoints(String payload);

// Fonction qui envoie une requête à une API qui calcule le trajet entre deux points GPS
String getApiUrl(String departureCoord, String destinationCoord);

// Fonction qui permet de mettre à jour à distance le programme sur l'ESP (en étant sur le même réseau wifi)
void updateESP();

#endif