#include "includes/functions.h"

#define M_PI 3.14159265358979323846

double convertRad(double input)
{
    return (M_PI * input) / 180;
};

double Distance(float lat_a_degre, float lon_a_degre, float lat_b_degre, float lon_b_degre)
{
    double R = 6378000; // Rayon de la terre en mètre
    return R * (M_PI / 2 - asin(sin(convertRad(lat_b_degre)) * sin(convertRad(lat_a_degre)) + cos(convertRad(lon_b_degre) - convertRad(lon_a_degre)) * cos(convertRad(lat_b_degre)) * cos(convertRad(lat_a_degre)))) / 1000;
};

int getRemainingPlaces(String payload)
{
    return payload.substring(payload.indexOf("<Free>") + 6, payload.indexOf("</Free>")).toInt();
};

double getDistanceBetweenPoints(String payload)
{
    // Il y a 3 fois le mot distance dans le json, on veut seulement récupérer le dernier, c'est pour cela qu'on a fait trois fois le substring sur le payload
    payload = payload.substring(payload.indexOf("distance") + 10, payload.indexOf(",\"summary\""));
    payload = payload.substring(payload.indexOf("distance") + 10, payload.indexOf(",\"summary\""));
    payload = payload.substring(payload.indexOf("distance") + 10, payload.indexOf(",\"summary\""));
    return payload.toDouble();
};

String getApiUrl(String departureCoord, String destinationCoord)
{
    return "https://router.project-osrm.org/route/v1/car/" + departureCoord + ";" + destinationCoord;
};

void updateESP()
{
    // Port defaults to 8266
    ArduinoOTA.setPort(8266);
    // Hostname defaults to esp8266-[ChipID]
    ArduinoOTA.setHostname("ProjetParkingESP");

    // No authentication by default
    // ArduinoOTA.setPassword("admin");

    ArduinoOTA.onStart([]()
                       { Serial.println("Start"); });
    ArduinoOTA.onEnd([]()
                     { Serial.println("\nEnd"); });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total)
                          { Serial.printf("Progress: %u%%\r", (progress / (total / 100))); });
    ArduinoOTA.onError([](ota_error_t error)
                       {
                           Serial.printf("Error[%u]: ", error);
                           if (error == OTA_AUTH_ERROR)
                               Serial.println("Auth Failed");
                           else if (error == OTA_BEGIN_ERROR)
                               Serial.println("Begin Failed");
                           else if (error == OTA_CONNECT_ERROR)
                               Serial.println("Connect Failed");
                           else if (error == OTA_RECEIVE_ERROR)
                               Serial.println("Receive Failed");
                           else if (error == OTA_END_ERROR)
                               Serial.println("End Failed");
                       });
    ArduinoOTA.begin();
    // Serial.println("Ready");
    // Serial.print("IP address: ");
    // Serial.println(WiFi.localIP());
}