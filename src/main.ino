#include <Arduino.h>
#include <ArduinoOTA.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>
#include <ESP8266WiFiMulti.h>
#include <string.h>
#include <algorithm>

// fonctions et classes
#include "includes/functions.h"
#include "includes/user.h"
#include "includes/parking.h"

// fichier de configuration
#include "config.h"

#define durationSleep 15 // nombre de secondes pour le temps du deep sleep
#define NB_TRYWIFI 5     // nombre d'essais de connection au wifi

// nom et mot de passe wifi enregistré dans le fichier config.h
const char *ssid = STASSID;
const char *password = STAPSK;

// Fingerprint pour la sécurité : Certificat SSL
const char *fingerprintdata3m = "234bcc8c0f9d6193179fbb06fb73da493723040b";
const char *fingerprintapi = "08f5de0beadc6d151a0267ebb54bb4299ab9dc67";

ESP8266WiFiMulti WiFiMulti;

Parking parkings[] = {
    /* ID,           nom,                      longitude,         latitude */
    Parking("FR_MTP_ANTI", "Antigone", 3.888818930000000, 43.608716059999999),
    Parking("FR_MTP_COME", "Comédie", 3.879761960000000, 43.608560920000002),
    Parking("FR_MTP_CORU", "Corum", 3.882257730000000, 43.613888209999999),
    Parking("FR_MTP_EURO", "Europa", 3.892530740000000, 43.607849710000004),
    Parking("FR_MTP_FOCH", "Foch Préfecture", 3.876570840000000, 43.610749120000001),
    Parking("FR_MTP_GAMB", "Gambetta", 3.871374360000000, 43.606951379999998),
    Parking("FR_MTP_GARE", "Saint Roch", 3.878550720000000, 43.603291489999997),
    Parking("FR_MTP_TRIA", "Triangle", 3.881844180000000, 43.609233840000002),
    Parking("FR_MTP_ARCT", "Arc de Triomphe", 3.873200750000000, 43.611002669999998),
    Parking("FR_MTP_PITO", "Pitot", 3.870191170000000, 43.612244939999997),
    Parking("FR_MTP_CIRC", "Circé Odysseum", 3.917849500000000, 43.604953770000002),
    Parking("FR_MTP_SABI", "Sabines", 3.860224600000000, 43.583832630000003),
    Parking("FR_MTP_GARC", "Garcia Lorca", 3.890715800000000, 43.590985089999997),
    Parking("FR_MTP_SABL", "Notre Dame de Sablassou", 3.922295360000000, 43.634191940000001),
    Parking("FR_MTP_MOSS", "Mosson", 3.819665540000000, 43.616237159999997),
    Parking("FR_STJ_SJLC", "Saint-Jean-le-Sec", 3.837931200000000, 43.570822249999999),
    Parking("FR_MTP_MEDC", "Euromédecine", 3.827723650000000, 43.638953590000000),
    Parking("FR_MTP_OCCI", "Occitanie", 3.848597960000000, 43.634562320000001),
    Parking("FR_CAS_CDGA", "Charles de Gaulle", 3.897762100000000, 43.628542119999999),
    Parking("FR_MTP_ARCE", "Arceaux", 3.867490670000000, 43.611716469999998),
    Parking("FR_MTP_POLY", "Polygone", 3.884765390000000, 43.608370960000002),
    Parking("FR_MTP_GA109", "Multiplexe (est)", 3.918980000000000, 43.605060000000000),
    Parking("FR_MTP_GA250", "Multiplexe (ouest)", 3.914030000000000, 43.604000000000000),
    // Les parkings ci-après n'ont pas de données temps réel sur le site de Montpellier 3M
    // On les a tout de même rajouté afin de les avoir pour une potentielle mise à jour de l'API
    Parking("", "Peyrou", 3.870383780000000, 43.611297000000000),
    Parking("", "Hôtel de ville", 3.895853270000000, 43.599231000000003),
    Parking("", "Jacou", 3.912884750000000, 43.654598700000001),
    Parking("", "Georges Pompidou", 3.921084190000000, 43.649339200000000),
    Parking("", "Via Domitia", 3.929538080000000, 43.646658010000003),
    Parking("", "Juvignac", 3.809621860000000, 43.617403740000000),
    Parking("", "Saint-Jean-de-Védas Centre", 3.830585520000000, 43.574962790000001),
    Parking("", "Lattes", 3.904817620000000, 43.570809879999999),
    Parking("", "Parc expo", 3.945678520000000, 43.572910210000003),
    Parking("", "Pérols centre", 3.957355560000000, 43.565378570000000),
    Parking("", "Décathlon", 3.923800380000000, 43.606185590000003),
    Parking("", "Ikéa", 3.925582560000000, 43.604609619999998),
    Parking("", "Géant Casino", 3.922104130000000, 43.603155600000001),
    Parking("", "Mare Nostrum", 3.919015140000000, 43.602370800000003),
    Parking("", "Végapolis", 3.914773710000000, 43.602896510000001),
    Parking("", "Multiplexe", 3.914110760000000, 43.604152429999999),
    Parking("", "La Mantilla", 3.902399940000000, 43.598772959999998),

};

void setup()
{
    Serial.begin(9600);

    for (uint8_t t = 4; t > 0; t--)
    {
        Serial.printf("[SETUP] WAIT %d...\n", t);
        Serial.flush();
        delay(1000);
    }

    WiFi.mode(WIFI_STA);
    WiFiMulti.addAP(ssid, password);

    int _try = 0;
    // on essaye de se connecter un certain nombre de fois au wifi, une fois cette limite passé, on entre en mode deep sleep pendant un certain temps
    while (WiFiMulti.run() != WL_CONNECTED)
    {
        Serial.print(".");
        delay(500);
        _try++;
        if (_try >= NB_TRYWIFI)
        {
            Serial.println("Impossible to connect WiFi network (Deep Sleep 15s)");
            // On peut activer un deep sleep de l'ESP pour réduire la consommation d'energie, pour cela, il faut connecter les broches RST et D0 après avoir téléversé le programme sur l'ESP
            // Décommenter la ligne ci-dessous pour l'utiliser.
            // ESP.deepSleep(durationSleep * 1000000);
        }
    }

    Serial.println("WiFi connected");

    // Pour la mise à jour du programme à distance (sur le même réseau wifi)
    updateESP();
}

void loop()
{
    // Pour la mise à jour de l'ESP via le réseau wifi
    ArduinoOTA.handle();

    if (!WiFiMulti.run() == WL_CONNECTED)
    {
        Serial.println("[HTTPS] Unable to connect\n");
        delay(500);
        return;
    }

    // Localisation de l'utilisateur
    User user = User(43.63286461170966, 3.846668930055735);

    // Nombre de parking
    int nbParkings = sizeof(parkings) / sizeof(parkings[0]);

    std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);

    client->setFingerprint(fingerprintapi);
    // Or, if you happy to ignore the SSL certificate, then use the following line instead:
    // client->setInsecure();
    HTTPClient https;

    // On formate les coordonnées de l'utilisateur pour pouvoir recevoir les données de l'API
    String userCoord = String(user.longitude, 6) + "," + String(user.lattitude, 6);
    for (int i = 0; i < nbParkings; i++)
    {
        // Concaténation des coordonnées du parking dans la variable parkingCoord
        String parkingCoord = String(parkings[i].longitude, 6) + "," + String(parkings[i].lattitude, 6);

        if (https.begin(*client, getApiUrl(userCoord, parkingCoord)))
        {
            int httpCode = https.GET();
            if (httpCode > 0)
            {
                if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY)
                {
                    parkings[i].distance = getDistanceBetweenPoints(https.getString());
                }
                else
                {
                    Serial.println("[Erreur] Récupération et calcul de distance. code http: " + httpCode);
                    // il y a une erreur dans le calcul de la distance donc on met -1
                    parkings[i].distance = -1;
                }
            }
            else
            {
                Serial.println("[Erreur] Problèmes de connexion avec l'API des distances. code http: " + httpCode);
                // il y a une erreur dans le calcul de la distance donc on met -1
                parkings[i].distance = -1;
            }
            https.end();
        }
        else
        {
            Serial.println("[Erreur] Problèmes de connexion avec l'API des distances. ");
            // il y a une erreur dans le calcul de la distance donc on met -1
            parkings[i].distance = -1;
        }
    }

    // On trie le tableau des parkings qui contient les distances
    std::sort(parkings, parkings + nbParkings);

    client->setFingerprint(fingerprintdata3m);
    // Or, if you happy to ignore the SSL certificate, then use the following line instead:
    // client->setInsecure();
    HTTPClient https2;
    for (int h = 0; h < nbParkings; h++)
    {
        // L'ID du parking doit être défini sinon cela signifie qu'il n'a pas de XmL associé
        if (parkings[h].id != "")
        {
            if (https2.begin(*client, parkings[h].getUrlXML())) // HTTPS
            {
                int httpCode = https2.GET(); // start connection and send HTTP header
                if (httpCode > 0)            // httpCode will be negative on error
                {
                    if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY)
                    {
                        parkings[h].nbPlacesDispo = getRemainingPlaces(https2.getString());
                    }
                    else
                    {
                        Serial.println("[Erreur http] Récupération du nombre de place restante dans le parking " + parkings[h].name);
                        // on continue même si il y a une erreur, on place 0 dans le nombre de places disponnibles pour ne pas prendre en compte ce parking
                        parkings[h].nbPlacesDispo = 0;
                    }
                }
                else
                {
                    Serial.println("[Erreur http] get xml sur le parking : " + parkings[h].name);
                    // on continue même si il y a une erreur, on place 0 dans le nombre de places disponnibles pour ne pas prendre en compte ce parking
                    parkings[h].nbPlacesDispo = 0;
                }
                https2.end();
            }
            else
            {
                Serial.println("[Erreur] Problèmes de connexion avec l'API qui récupère le nombre de places restantes sur le parking :" + parkings[h].name);
                // on continue même si il y a une erreur, on place 0 dans le nombre de places disponnibles pour ne pas prendre en compte ce parking
                parkings[h].nbPlacesDispo = 0;
            }
        }
    }

    // On selectionne les 3 parkings les plus proches
    Serial.println("Les trois parkings les plus proches : ");
    int count = 0;
    for (int i = 0; i < nbParkings; i++)
    {
        if (parkings[i].id && parkings[i].nbPlacesDispo >= 1 && parkings[i].distance >= 0)
        {
            Serial.println("  -> " + parkings[i].name + ", avec " + parkings[i].nbPlacesDispo + " place(s) libre à " + parkings[i].distance + " mètre(s) de moi.");
            count++;
            if (count == 3) // Alors on a selectionné les 3 parkings les plus proches
            {
                break; // on sort de la boucle for
            }
        }
    }
    if (count == 0)
    {
        Serial.println("Aucun parking de disponible.");
    }

    Serial.println("\n");
    Serial.println("[ Prochains résultats dans 15 secondes ... ]");
    // On peut activer un deep sleep de l'ESP pour réduire la consommation d'energie, pour cela, il faut connecter les broches RST et D0 après avoir téléversé le programme sur l'ESP
    // Décommenter la ligne ci-dessous pour l'utiliser.
    // ESP.deepSleep(durationSleep * 1000000);

    // il faut commenter les trois lignes suivante si on utilise Deep Sleep de l'ESP, sinon on utilise seulement la coupure du WIFI (il n'y a pas besoin de relié des broches)
    WiFi.setSleepMode(WIFI_MODEM_SLEEP);
    delay(15000);
    WiFi.setSleepMode(WIFI_NONE_SLEEP);
}
