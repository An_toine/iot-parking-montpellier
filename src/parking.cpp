#include "includes/parking.h"

// Constructor
Parking::Parking(String id, String name, float longitude, float lattitude)
{
    this->id = id;
    this->name = name;
    this->longitude = longitude;
    this->lattitude = lattitude;
};

// Méthode Pour obtenir l'url du xml
String Parking::getUrlXML()
{
    return "https://data.montpellier3m.fr/sites/default/files/ressources/" + this->id + ".xml";
}

// Comparator
bool Parking::operator<(Parking const &other)
{
    return distance < other.distance;
}